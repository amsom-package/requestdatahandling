<?php return array(
    'root' => array(
        'name' => 'amsom-habitat/request-data-handling',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => 'ff1bfaddc98eb6652362ac71b2ed6a2582fe5c40',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'amsom-habitat/request-data-handling' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'ff1bfaddc98eb6652362ac71b2ed6a2582fe5c40',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
