<?php

namespace AmsomUtilities;

class RequestDataHandling
{
    /*
     * @example normalizeArrayData($data,[ "exception" => ["quote" => ['individu>id'], "null" => ["label"]]], "directive" => [])
     * @example normalizeArrayData($data,[ "exception" => ["quote" => ['individu>id'], "null" => ["label"]]])
     * @example normalizeArrayData($data,[ "directive" => ["json" => ['individu>modification']]])
     *
     * @param data $data qui va contenir le tableau à normaliser
     * @param exception $exception qui est facultatif et va contenir les exception
     * @param parentPath $parentPath qui est facultatif et va contenir le chemin parent
     *
     * @return array
     */
    public function normalizeArrayData($data, $rules = [], $parentPath = '')
    {
        $exception = $rules['exception'] ?? [];
        $directive = $rules['directive'] ?? [];

        foreach ($data as $key => $value) {
            $path = $parentPath . $key;

            // Si le chemin de propriété a été escape de manière global on ne la traite meme pas, on renvoie sa valeur
            if (array_key_exists("global", $exception) && in_array($path, $exception['global'])) {
                $data[$key] = $value;
            } // Si c'est un tableau, on alimente le chemin et lance de manière récursive
            else {
                if (is_array($value)) {
                    $value = $this->normalizeArrayData($value, $exception, ($path . '>'));
                }
                //Normalisation selon exceptions

                // Si c'est un null et qu'il n'est pas escape on le passe de null à ''
                if ((!array_key_exists("null", $exception) || !in_array($path, $exception['null'])) && $value === null)
                    $value = strval($value);

                // Si c'est une chaine de caractère et qu'il n'est pas escape on va doubler ses quote
                if (((!array_key_exists("quote", $exception) || !in_array($path, $exception['quote']))) && is_string($value))
                    $value = str_replace("'", "''", $value);

                // Si c'est une chaine de caractère et qu'il n'est pas escape on va doubler ses quote
                if (((!array_key_exists("bool", $exception) || !in_array($path, $exception['bool']))) && gettype($value) === 'boolean')
                    $value = $value === true ? 1 : 0;

                //Normalisation selon directives
                if ((array_key_exists("json", $directive) && in_array($path, $directive['json']))) {
                    $json = json_encode($value, JSON_UNESCAPED_UNICODE);
                    // $json = str_replace("'", "''", $json ?? ""); // remplace ' par '' pour Oracle SQL
                    $value = $json === "[]" ? null : $json;
                }
                
                // Permet de forcer la casse en uppercase
                if (array_key_exists("uppercase", $directive) && in_array($path, $directive['uppercase']) && is_string($value))
                $value = strtoupper($value);
            
                // Permet de forcer la casse en lowercase
                if (array_key_exists("lowercase", $directive) && in_array($path, $directive['lowercase']) && is_string($value))
                    $value = strtolower($value);

                /*
                 * todo, on peut peut etre trim aussi voir meme upper et/ou lower soit par defaut
                 * soit via un autre tableau rule (qui fonctionne comme exemption mais force à l'inverse de exception)
                 */

                $data[$key] = $value;
            }
        }

        return $data;
    }
}
